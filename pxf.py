import socket, random
UDP_IP = "10.10.9.127"
UDP_PORT = 9930
target = (UDP_IP, UDP_PORT)
sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sendto = sock.sendto
def setpx(x,y,r,g,b):
  msg = (x//256, x%256, y//256, y%256, r%255, g%256, b%256)
  msg = map(chr, msg)
  msg = ''.join(msg)
  sendto(msg, target)

for y in range(1024):
  for x in range(1024):
    setpx(x,y,y,x,y+x)
  
