/* Sample UDP client */

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>

#define MAXMSG 1400

int main(int argc, char**argv)
{
   int sockfd, n;
   struct sockaddr_in servaddr, cliaddr;
   char sendline[MAXMSG];

   sockfd = socket(AF_INET,SOCK_DGRAM,0);

   bzero(&servaddr, sizeof(servaddr));
   servaddr.sin_family = AF_INET;
   servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
   servaddr.sin_port = htons(9930);

   int x, y, i=0;
   sendline[i++] = 0;
   sendline[i++] = 0;
   sendline[i++] = 0;
   sendline[i++] = 0;
   for(y=0; y<1280; y++) {
     for(x=0; x<1920; x++) {
       if(i >= MAXMSG) {
         sendto(sockfd, sendline, i, 0, (struct sockaddr *)&servaddr, sizeof(servaddr));
         usleep(199);
         i=0;
         sendline[i++] = x>>8;
         sendline[i++] = x;
         sendline[i++] = y>>8;
         sendline[i++] = y;
       }
       sendline[i++] = x * 256 / 1940;
       sendline[i++] = y * 256 / 1280;
       sendline[i++] = x+y;
     }
   }
   if(i) sendto(sockfd, sendline, i, 0, (struct sockaddr *)&servaddr, sizeof(servaddr));
}
