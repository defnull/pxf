#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memset */

#define BUFLEN 1500
#define PORT 9930

void diep(char *s)
{
  perror(s);
  exit(1);
}

struct fbinfo {
  int fd;
  unsigned char * mmap;
  int mlen;
  struct fb_var_screeninfo info;
} fb;

void setup_fb(struct fbinfo* fb) {
   fb->fd = open("/dev/fb0", O_RDWR);
   ioctl(fb->fd, FBIOGET_VSCREENINFO, &(fb->info));
   fb->mlen = fb->info.xres * fb->info.yres * (fb->info.bits_per_pixel / 8);
   fb->mmap = (unsigned char*) mmap(0, fb->mlen, PROT_READ | PROT_WRITE, MAP_SHARED, fb->fd, 0);
}

void clear(struct fbinfo* fb) {
   memset(fb->mmap, 0, fb->mlen);
}

void draw(struct fbinfo* fb, int x, int y, char r, char g, char b) {
   int index = (y * fb->info.xres + x) * (fb->info.bits_per_pixel/8);
   if(index >= fb->mlen) return;
   fb->mmap[index+0] = r;
   fb->mmap[index+1] = g;
   fb->mmap[index+2] = b;
}

int main(int argc, char **argv) {
   struct fbinfo fb;
   setup_fb(&fb);
   clear(&fb);
   // Beende, wenn die Farbaufloesung nicht 8 Bit pro Pixel entspricht
   if(fb.info.bits_per_pixel != 24) {
     printf("Muh %d\n", fb.info.bits_per_pixel);
     return 0;
   }

   struct sockaddr_in si_me, si_other;
   int s, i, slen=sizeof(si_other);
   char buf[BUFLEN];

   if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1)
     diep("socket");

   si_me.sin_family = AF_INET;
   si_me.sin_port = htons(PORT);
   si_me.sin_addr.s_addr = htonl(INADDR_ANY);
   if (bind(s, (struct sockaddr *) &si_me, sizeof(si_me))==-1)
     diep("bind");

   while (1) {
     memset(buf, 0, BUFLEN);
     ssize_t msglen = recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen);
     if(msglen == -1) diep("recvfrom()");
     if(msglen < 7) continue;
     int x = (buf[0] << 8) | buf[1];
     int y = (buf[2] << 8) | buf[3];
     int off = 4;
     while(off+3 <= msglen) {
       draw(&fb, x++, y, buf[off++], buf[off++], buf[off++]);
     }
   }

   // Zeiger wieder freigeben
   munmap(fb.mmap, fb.mlen);

   // Rückgabewert
   return 0;
}
