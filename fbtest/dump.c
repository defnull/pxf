/* Sample UDP client */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memset */

#define MAXMSG 1400

struct fbinfo {
  int fd;
  unsigned char * mmap;
  int mlen;
  struct fb_var_screeninfo info;
} fb;

void setup_fb(struct fbinfo* fb) {
   fb->fd = open("/dev/fb0", O_RDWR);
   ioctl(fb->fd, FBIOGET_VSCREENINFO, &(fb->info));
   fb->mlen = fb->info.xres * fb->info.yres * (fb->info.bits_per_pixel / 8);
   fb->mmap = (unsigned char*) mmap(0, fb->mlen, PROT_READ | PROT_WRITE, MAP_SHARED, fb->fd, 0);
}




int main(int argc, char**argv)
{
   int sockfd, n;
   struct sockaddr_in servaddr, cliaddr;
   char sendline[MAXMSG];

   sockfd = socket(AF_INET,SOCK_DGRAM,0);

   bzero(&servaddr, sizeof(servaddr));
   servaddr.sin_family = AF_INET;
   servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
   servaddr.sin_port = htons(9930);

   struct fbinfo fb;
   setup_fb(&fb);
   int x, y, i=0;

   sendline[i++] = 0;
   sendline[i++] = 0;
   sendline[i++] = 0;
   sendline[i++] = 0;

   for(x=0; x<fb.info.xres; x++) {
     for(y=0; y<fb.info.yres; y++) {
       int index = (y * fb.info.xres + x) * (fb.info.bits_per_pixel/8);
       if(i >= MAXMSG) {
         sendto(sockfd, sendline, i, 0, (struct sockaddr *)&servaddr, sizeof(servaddr));
         i=0;
         sendline[i++] = x>>8;
         sendline[i++] = x;
         sendline[i++] = y>>8;
         sendline[i++] = y;
       }
       sendline[i++] = fb.mmap[index++];
       sendline[i++] = fb.mmap[index++];
       sendline[i++] = fb.mmap[index++];
     }
   }
   if(i) sendto(sockfd, sendline, i, 0, (struct sockaddr *)&servaddr, sizeof(servaddr));
}
